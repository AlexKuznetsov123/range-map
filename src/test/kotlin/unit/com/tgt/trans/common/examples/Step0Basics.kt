package com.tgt.trans.common.examples

import com.tgt.trans.common.rangemap.rangeMapFrom
import org.junit.Test
import org.junit.jupiter.api.assertAll
import kotlin.test.assertEquals
import kotlin.test.assertFalse

class Step0Basics {
    val sut = rangeMapFrom(-30..10 to "Normal", 10..20 to "Mild", 20..25 to "Very Mild")

    @Test
    fun getValue() {
        for(degreesInF in 5..25) {
            println("$degreesInF is ${sut[degreesInF].get()}")
        }
        assertAll(
                { assertEquals ("Normal", sut[9].get()) },
                { assertEquals ("Mild", sut[10].get()) }
        )
    }

    @Test
    fun returnsEmpty() {
        assertAll(
                { assertFalse { sut[-31].isPresent } },
                { assertFalse { sut[26].isPresent } }
        )
    }
}