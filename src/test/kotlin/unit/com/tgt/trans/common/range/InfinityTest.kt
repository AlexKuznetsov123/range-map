package com.tgt.trans.common.range

import org.junit.Test
import org.junit.jupiter.api.assertAll
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class InfinityTest {
    val plusInfinity = PlusInfinity<Int>()
    val other = RangeEdge(1, true, EdgeSide.END)
    val minusInfinity = MinusInfinity<Int>()

    @Test
    fun minWorks() {
        assertAll(
                { assertEquals(other, plusInfinity.min(other)) },
                { assertEquals(other, other.min(plusInfinity)) },
                { assertEquals(minusInfinity, minusInfinity.min(other)) },
                { assertEquals(minusInfinity, other.min(minusInfinity)) },
                { assertEquals(minusInfinity, plusInfinity.min(minusInfinity)) },
                { assertEquals(minusInfinity, minusInfinity.min(plusInfinity)) }
        )
    }

    @Test
    fun maxWorks() {
        assertAll(
                { assertEquals(plusInfinity, plusInfinity.max(other)) },
                { assertEquals(plusInfinity, other.max(plusInfinity)) },
                { assertEquals(other, minusInfinity.max(other)) },
                { assertEquals(other, other.max(minusInfinity)) },
                { assertEquals(plusInfinity, plusInfinity.max(minusInfinity)) },
                { assertEquals(plusInfinity, minusInfinity.max(plusInfinity)) }
        )
    }

    @Test
    fun compareToWorks() {
        assertAll(
                { assertEquals(0, plusInfinity.compareTo(plusInfinity)) },
                { assertEquals(0, minusInfinity.compareTo(minusInfinity)) },
                { assertTrue(minusInfinity < other) },
                { assertTrue(minusInfinity < plusInfinity) },
                { assertTrue(other < plusInfinity) }
        )
    }
}