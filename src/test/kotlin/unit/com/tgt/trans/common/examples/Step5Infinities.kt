package com.tgt.trans.common.examples

import com.tgt.trans.common.range.*
import com.tgt.trans.common.rangemap.mutableRangeMapOf
import com.tgt.trans.common.rangemap.rangeMapOf
import org.junit.Test
import kotlin.test.assertEquals

class Step5Infinities {
    val lowValue = "Low"
    val highValue = "High"
    val threshold = 20

    @Test
    fun greaterThanExample() {
        val sut = rangeMapOf(noGreaterThan(threshold) to lowValue,
                greaterThan(threshold) to highValue)
        assertEquals(lowValue, sut[threshold-1].get())
        assertEquals(lowValue, sut[threshold].get())
        assertEquals(highValue, sut[threshold+1].get())
    }

    @Test
    fun lessThanExample() {
        val sut = rangeMapOf(lessThan(threshold) to lowValue,
                noLessThan(threshold) to highValue)
        assertEquals(lowValue, sut[threshold-1].get())
        assertEquals(highValue, sut[threshold].get())
        assertEquals(highValue, sut[threshold+1].get())
    }

    @Test
    fun allValuesExample() {
        val sut = rangeMapOf(allValues<Int>() to lowValue, threshold closedClosed threshold to highValue)
        assertEquals(lowValue, sut[threshold-1].get())
        assertEquals(highValue, sut[threshold].get())
        assertEquals(lowValue, sut[threshold+1].get())
    }
}