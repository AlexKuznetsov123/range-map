package com.tgt.trans.common.range

class EmptyRange<T: Comparable<T>> : IRange<T> {
    override fun and(other: IRange<T>) = this

    override fun contains(value: T) = false

    override fun isBefore(other: IRange<T>) = false

    override fun isNotEmpty(): Boolean = false

    override fun intersect(other: IRange<T>) = false
}